#!/bin/bash


if [ $# -ne 1 ]; then
    echo "The wrong way"
    echo "Usage: $0 <directory>"
    exit 1
fi


if [ ! -d "$1" ]; then
    echo "Error: Directory '$1' does not exist."
    exit 1
fi


directory="$1"

current_datetime=$(date +"%Y-%m-%d_%H-%M")

archive_name="backup_${current_datetime}.tar.gz"

tar -czf "$archive_name" "$directory"
pwd=$(pwd)

echo "Backup created: ${pwd}/${archive_name}"

echo "Archive size: $(stat -c %s "$archive_name") bytes"
K B - 3 0 8   i s   t h e   b e s t  
 